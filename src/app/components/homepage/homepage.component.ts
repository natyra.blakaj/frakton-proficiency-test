import {Component, ElementRef, OnChanges, OnInit, ViewChild} from '@angular/core';
import {BaseService} from '../../services/base.service';
import {CustomElementsModel} from '../../models/CustomElementsModel';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  @ViewChild('mainElement') mainElement: ElementRef = new ElementRef<any>('mainElement');
  allElements: CustomElementsModel[] = [];
  hierarchicalElements: CustomElementsModel[] = [];
  summaryList: Array<Array<string>> = [];
  summaryLink = '';

  constructor(
    private baseService: BaseService
  ) {
  }

  ngOnInit(): void {
    this.getData();
  }

  getElementChildren(parentElement: CustomElementsModel): void {
    if (parentElement.checked && parentElement.hasChildren) {
      parentElement.children?.forEach(childrenOfParent => {
        this.getElementChildren(childrenOfParent);
      });
    } else if (!parentElement.children?.length && parentElement.checked) {
      const newArray: string[] = [];
      newArray.push(parentElement.name);
      this.getParent(parentElement.parent, newArray);
    } else if (!parentElement.checked) {
      this.removeOldArray(parentElement.name);
    }
  }

  getParent(id: number | undefined, array: string[]): void {
    const parent = this.allElements.find(element => element.id === id);
    if (parent) {
      array.push(parent.name);
    }
    if (parent?.parent === 0) {
      this.summaryList.push(array.reverse());
    } else {
      this.getParent(parent?.parent, array);
    }

    this.summaryList = this.checkForDuplicates(this.summaryList);
  }

  showElements(): void {
    setTimeout(() => {
      this.hierarchicalElements.forEach(hierarchicalElement => {
        this.getElementChildren(hierarchicalElement);
      });
    });
  }

  removeOldArray(unselectedElement: string): void {
    setTimeout(() => {
      this.summaryList.forEach((summaryElement) => {
        if (summaryElement.includes(unselectedElement)) {
          this.summaryList = this.summaryList.filter(arrayIt => arrayIt !== summaryElement);
        }
      });
    });
  }

  getData(): void {
    this.baseService.getAllElements().subscribe((response) => {
      this.allElements = response;
      this.allElements.forEach(element => {
        if (element.parent === 0) {
          this.hierarchicalElements.push(element);
        }
      });
      this.createHiearchiealObject(this.hierarchicalElements);
    });
  }

  createHiearchiealObject(childrenArray: CustomElementsModel[]): void {
    childrenArray.forEach(childrenElement => {
      const newChildren: CustomElementsModel[] | null | undefined = [];
      this.allElements?.forEach(secondIterator => {
        secondIterator.checked = false;
        if (childrenElement.id === secondIterator.parent) {
          childrenElement.hasChildren = true;
          newChildren.push(secondIterator);
          childrenElement.children = newChildren;
          this.createHiearchiealObject(childrenElement.children);
        }
      });
    });
  }

  checkForDuplicates(arr: string | any[]): any[] {
    const uniques = [];
    const itemsFound = {};
    for (let i = 0, l = arr.length; i < l; i++) {
      const stringified = JSON.stringify(arr[i]);
      // @ts-ignore
      if (itemsFound[stringified]) {
        continue;
      }
      uniques.push(arr[i]);
      // @ts-ignore
      itemsFound[stringified] = true;
    }

    return uniques;
  }

  saveSummary(): void {
    const id = localStorage.length + 1;
    localStorage.setItem(id.toString(), JSON.stringify(this.summaryList));
    this.summaryLink = `${environment.baseUrl}/summary/` + id;
  }
}
