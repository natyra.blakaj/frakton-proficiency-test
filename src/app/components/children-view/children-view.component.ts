import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-children-view',
  templateUrl: './children-view.component.html',
  styleUrls: ['./children-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChildrenViewComponent implements OnInit {
  @Input() elements: any;
  constructor() { }

  ngOnInit(): void {
  }

}
