import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-summary-display',
  templateUrl: './summary-display.component.html',
  styleUrls: ['./summary-display.component.scss']
})
export class SummaryDisplayComponent implements OnInit {
  id = 0;
  activeSummary: string[] = [];
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = params.id;
      // @ts-ignore
      this.activeSummary = JSON.parse(localStorage.getItem(params.id));
    });
  }

}
