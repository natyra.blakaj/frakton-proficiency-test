import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomepageComponent} from './components/homepage/homepage.component';
import {SummaryDisplayComponent} from './components/summary-display/summary-display.component';

const routes: Routes = [
  {
    path: 'summary/:id',
    component: SummaryDisplayComponent
  },
  {
    path: '',
    component: HomepageComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
