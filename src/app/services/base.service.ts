import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CustomElementsModel} from '../models/CustomElementsModel';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  private readonly baseUrl = environment.baseApiUrl + '/articles.php';

  constructor(private http: HttpClient) {
  }

  getAllElements(): Observable<CustomElementsModel[]> {
    return this.http.get<CustomElementsModel[]>(this.baseUrl);
  }
}
