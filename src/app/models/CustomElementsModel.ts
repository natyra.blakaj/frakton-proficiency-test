export interface CustomElementsModel {
  id: number | undefined;
  name: string;
  parent: number;
  checked?: boolean;
  hasChildren?: boolean;
  children?: CustomElementsModel[] | null;
}
