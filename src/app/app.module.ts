import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomepageComponent} from './components/homepage/homepage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BaseService} from './services/base.service';
import {ChildrenViewComponent} from './components/children-view/children-view.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {SummaryDisplayComponent} from './components/summary-display/summary-display.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    ChildrenViewComponent,
    SummaryDisplayComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    FormsModule,
  ],
  providers: [BaseService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
